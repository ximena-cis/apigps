from flask import Flask, jsonify, request
import firebase_admin
from firebase_admin import credentials, firestore
from datetime import datetime
from google.cloud.firestore_v1 import GeoPoint
from dotenv import load_dotenv
import os

load_dotenv()

cred = credentials.Certificate(
    {
        "type": os.getenv("TYPE"),
        "project_id": os.getenv("PROJECT_ID"),
        "private_key_id": os.getenv("PRIVATE_KEY_ID"),
        "private_key": os.getenv("PRIVATE_KEY"),
        "client_email": os.getenv("CLIENT_EMAIL"),
        "client_id": os.getenv("CLIENT_ID"),
        "auth_uri": os.getenv("AUTH_URI"),
        "token_uri": os.getenv("TOKEN_URI"),
        "auth_provider_x509_cert_url": os.getenv("AUTH_PROVIDER_X509_CERT_URL"),
        "client_x509_cert_url": os.getenv("CLIENT_X509_CERT_URL"),
        "universe_domain": os.getenv("UNIVERSE_DOMAIN"),
    }

)
firebase_admin.initialize_app(cred)

db = firestore.client()
gps_ref = db.collection('gps').document('ZTyTDN7cp6RDljiV0DHI')
routes_ref = db.collection('routes')

app = Flask(__name__)


base = '/api/gps/'


def create_data(lat, lng):
    geopoint = GeoPoint(lat, lng)   # Crea un GeoPoint

    data = {
        'location': geopoint,
        'timestamp': datetime.now()
    }
    gps_ref.set(data)  # Update data

    routes_ref.document().set(data)  # Save routes


@app.route("/")
def home():
    return "Hello human! This is my landing page for my GPS API =D"


@app.route(base, methods=['POST'])
def set_route():
    data = request.json
    lat = float(data.get('lat'))
    lng = float(data.get('lng'))

    if lat is None or lng is None:
        return jsonify({"error": "Missing lat or lng"}), 400

    create_data(lat, lng)
    return jsonify({"success": True}), 200


if __name__ == '__main__':
    app.run(debug=True)
